require 'positionable'
include ToyRobot

RSpec.describe Positionable do
  it "shouldn't be falling" do
    [[0, 2, :west],
     [2, 4, :north],
     [4, 2, :east],
     [2, 0, :south]].each do |cs|
      p = Positionable.new
      p.x = cs[0]
      p.y = cs[1]
      p.direction = cs[2]

      expect do
        p.move!
      end.to change { p.x }.by(0).and change { p.y }.by(0)
    end
  end

  context "it should be movable" do 
      before :each do 
        @pos = Positionable.new
        @pos.x = 3
        @pos.y = 3
      end

      %i[north east south west].each do |dir|
          it "to #{dir}" do
              expect do
                  @pos.direction = dir
                  @pos.move!
              end.to change {@pos.x}.or change{@pos.y}

          end
      end

  end
end
