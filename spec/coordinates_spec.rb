require 'positionable'
include ToyRobot

RSpec.describe Coordinates do
  context 'moving' do
    before(:each) do
      @c = Coordinates.new
      @c.x = 3
      @c.y = 3
    end

    it 'should be movable' do
      expect do
        @c.move_east!
      end.to change { @c.x }.by(1)

      expect do
        @c.move_west!
      end.to change { @c.x }.by(-1)

      expect do
        @c.move_north!
      end.to change { @c.y }.by(1)

      expect do
        @c.move_south!
      end.to change { @c.y }.by(-1)
    end

    it "shouldn't be valid out of field" do
      [[-1, 0], [0, -1], [5, 0], [0, 5]].each do |cs|
        expect(Coordinates.valid?(*cs)).to be == false
      end
    end
  end
end
