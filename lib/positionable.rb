module ToyRobot
  class Coordinates
    attr_accessor :x, :y, :verbose

    def move!(direction)
      sym = direction.is_a?(Symbol) ? direction : direction.downcase.to_sym
      x = self.x
      y = self.y
      case sym
      when :north
        y += 1
      when :south
        y -= 1
      when :west
        x -= 1
      when :east
        x += 1
      end

      puts "DEBUG: trying to move #{direction}" if @verbose
      if Positionable.valid?(x, y)
        self.x = x
        self.y = y
        puts "DEBUG: moved to #{x}, #{y}" if @verbose
        return self
      else
        puts "DEBUG: can't move" if @verbose
        return false
      end
    end

    %i[north south west east].each do |d|
      eval %{
          def move_#{d}!
              move!(:#{d})
          end
      }
    end

    def self.valid?(x, y)
      x >= 0 && x < 5 && y >= 0 && y < 5
    end
  end

  class Positionable < Coordinates
    attr_accessor :direction_index

    @@directions = %i[north east south west]

    def move!
      super direction
    end

    def rotate!(offset)
      self.direction_index = (direction_index + offset) % @@directions.length
      self
    end

    def turn_left!
      rotate!(-1)
      puts "DEBUG: turned left to #{direction}" if @verbose
    end

    def turn_right!
      rotate!(1)
      puts "DEBUG: turned right to #{direction}" if @verbose
    end

    def direction
      @@directions[@direction_index]
    end

    def direction=(facing)
      i = @@directions.find_index(facing.downcase.to_sym)
      self.direction_index = i if i
    end
  end
end
