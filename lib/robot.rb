require './lib/positionable.rb'
module ToyRobot
  class Robot < Positionable
    def initialize
      @ontable = false
    end

    def place(x, y, facing)
      return unless Positionable.valid?(x, y)
      self.x = x
      self.y = y
      self.direction = facing
      @ontable = true
    end

    def perform_command(line)
      case line
      when /^place ([0-9]+),([0-9]+),([a-z]+)/i
        arr = line.match(/place ([0-9]+),([0-9]+),([a-z]+)/i)
        x = arr[1].to_i
        y = arr[2].to_i
        facing = arr[3]
        place(x, y, facing)
      when /^right$/i
        turn_right! if @ontable
      when /^left$/i
        turn_left! if @ontable
      when /^move$/i
        move! if @ontable
      when /^report$/i
        report if @ontable
      end
    end

    def <<(line)
      perform_command line
    end

    def read_file(filename)
      f = File.open(filename)
      f.each_line do |line|
        perform_command line
      end
      f.close
    end

    def report
      puts "(#{x}, #{y}) facing #{direction.upcase}"
    end
  end
end
