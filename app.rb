require './lib/robot.rb'
include ToyRobot

if ARGV.empty? || ARGV.include?('-h')
  puts %(
      Usage: ruby app.rb -f "filename" [--verbose]
  )
  return
end

r = Robot.new
r.verbose = true if ARGV.include? '--verbose'
i = ARGV.find_index('-f')
if i
  filename = ARGV[i + 1]
  r.read_file filename
end
